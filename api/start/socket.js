'use strict'

/*
|--------------------------------------------------------------------------
| Websocket
|--------------------------------------------------------------------------
|
| This file is used to register websocket channels and start the Ws server.
| Learn more about same in the official documentation.
| https://adonisjs.com/docs/websocket
|
| For middleware, do check `wsKernel.js` file.
|
*/


const Ws = use('Ws')
const Server = use('Server')
const io = use('socket.io')(Server.getInstance())

io.use((socket, next) => {
    console.log('middleware', socket.handshake.headers['authorization']);
    return next()
})
io.sockets.on('connection', (socket, username) => {
    socket.on('ubicacion',(ubicacion) => {
        // console.log('ubicacion => ', ubicacion);
        socket.emit('ubicacion', ubicacion)
    });
});
  
Ws.channel('chat', 'UbicacionController')

