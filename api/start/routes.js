'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')

// Users
Route.group(() => {
  Route.post('/auth', 'UserController.login');
}).prefix('/api/users');

//ciudadanos
Route.group(() => {
  Route.post("/", "CiudadanoController.store");
}).prefix("/api/ciudadanos");

// recolectores
Route.group(() => {
  Route.post('/', 'RecolectorController.store'); // falta auth ¿Quien registra recolectores?
}).prefix('/api/recolectores');

// seguimientos
Route.group(() => {
  Route.post('/', 'SeguimientoController.store').middleware(['auth'])
  Route.get('/', 'SeguimientoController.findAll').middleware(['auth'])
}).prefix('/api/seguimientos');


