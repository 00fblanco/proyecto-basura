'use strict'
const User = use('App/Models/User')
const Recolector = use('App/Models/Recolector')
const Database = use('Database');

const { validate } = use('Validator');

class RecolectorController {
    async store({ request, response, auth }){

        // user validate
        const userData = request.only(['correo', 'contrasenia']);
        const validationUser = await validate({...userData, rol: 'recolector'}, User.storeRules, User.errorMessages);

        //recolector validate
        const recolectorData = request.only(['nombre', 'ap_paterno', 'ap_materno']);
        const validationRecolector = await validate(recolectorData, Recolector.storeRules, Recolector.errorMessages);
        
        if(validationRecolector.fails()){
            response.status(401).json({
                messages: validationRecolector.messages()
            })
        }else if(validationUser.fails()){
            response.status(401).json({
                messages: validationUser.messages()
            })
        }else{
            const trx = await Database.beginTransaction()
                // Guardamos al usuario
                const user = await User.create({ ...userData, rol: 'recolector' }, trx);
                // Guardamos los datos del recolector
                const recolector = await Recolector.create({...recolectorData, user_id: user.id }, trx);
                response.status(201).json({ user, recolector });
            trx.commit();
        }
    }
}

module.exports = RecolectorController
