'use strict'
const User = use("App/Models/User");
const Ciudadano = use("App/Models/Ciudadano");
const Database = use("Database");

const { validate } = use("Validator");

class CiudadanoController {
    async store({ request, response, auth }) {
        // user validate
        const userData = request.only(["correo", "contrasenia"]);
        const validationUser = await validate({ ...userData, rol: "ciudadano" }, User.storeRules, User.errorMessages);
        //Ciudadano validate
        const ciudadanoData = request.only(["nombre", "ap_paterno", "ap_materno"]);
        const validationCiudadano = await validate(ciudadanoData, Ciudadano.storeRules, Ciudadano.errorMessages);
    
        if (validationCiudadano.fails()) {
            response.status(401).json({
                messages: validationCiudadano.messages()
            });
        } else if (validationUser.fails()) {
            response.status(401).json({
                messages: validationUser.messages()
            });
        } else {
          const trx = await Database.beginTransaction();
          // Guardamos al usuario
          const user = await User.create({ ...userData, rol: "ciudadano" }, trx);
          // Guardamos los datos del ciudadano
          const ciudadano = await Ciudadano.create({ ...ciudadanoData, user_id: user.id }, trx );
          response.status(201).json({ user, ciudadano });
          trx.commit();
        }
      }
}

module.exports = CiudadanoController
