'use strict'
const Seguimiento = use('App/Models/Seguimiento')
const { validate } = use('Validator');

class SeguimientoController {

    async store({ request, response, auth}){
        // validar es que no tenga seguimientos sin terminar
        const recolector = await auth.user.recolector().fetch();
        const seguimiento = await Seguimiento.create({ recolector_id: recolector.id });
        response.status(201).json({ seguimiento });
    }

    async findAll({ request, response, auth }){
        const recolector = await auth.user.recolector().fetch();
        const seguimientos = await Seguimiento.query().where('recolector_id', recolector.id);
        response.status(200).json({ seguimientos });
    }
}

module.exports = SeguimientoController
