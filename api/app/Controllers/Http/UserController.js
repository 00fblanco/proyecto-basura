'use strict'
const User = use('App/Models/User');
const Ciudadano = use('App/Models/Ciudadano');
const Recolector = use('App/Models/Recolector');
const { validate } = use('Validator');
const Hash = use('Hash');

class UserController {
    async login({ request, response, auth }){
        const { correo, contrasenia } = request.post();
        const user = await User.findBy('correo', correo);
        if(!user){
            return response.status(404).json({messages: 'No se encontro el correo del usuario.'});
        }
        const verifyPassword = await Hash.verify(contrasenia, user.contrasenia);
        if(verifyPassword){
            const token = await auth.generate(user);
            if(user.rol === 'ciudadano'){
                const ciudadano = await Ciudadano.findBy('user_id', user.id);
                return response.status(201).json({ token, user, ciudadano })
            }else if(user.rol === 'recolector'){
                const recolector = await Recolector.findBy('user_id', user.id);
                return response.status(201).json({ token, user, recolector })
            }else{
                return response.status(500).json({ messages: 'Ups ocurrio un error :c intenta mas tarde'});
            }
        }else{
            return response.status(404).json({ messages: 'La contraseña es incorrecta' });
        }
    }
}

module.exports = UserController
