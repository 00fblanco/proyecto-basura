'use strict'

const Model = use('Model')

class Ubicacion extends Model {
    static get table () {
        return 'ubicaciones'
    }
}

module.exports = Ubicacion
