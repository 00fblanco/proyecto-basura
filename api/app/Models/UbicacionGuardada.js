'use strict'

const Model = use('Model')

class UbicacionGuardada extends Model {

    static get table () {
        return 'ubicaciones_guardadas'
    }
}

module.exports = UbicacionGuardada
