'use strict'

const Model = use('Model')

class Seguimiento extends Model {
    static get storeRules(){
        return {
          fecha_inicio: 'required|dateFormat:YYYY-MM-DD HH:mm:ss',
          fecha_termino: 'dateFormat:YYYY-MM-DD HH:mm:ss',
        }
    }
    static get errorMessages(){
        return {
            'alpha': '{{field}} debe ser solo letras',
            'required': '{{field}} es requerido',
            'email': '{{field}} debe tener formato de un correo, ejemplo: 00fblanco@dev.com',
            'min': '{{field}} debe tener como minimo {{argument.0}} caracteres',
            'max': '{{field}} debe tener como maximo {{argument.1}} caracteres',
            'integer': "{{field}} debe ser numerico entero",
            'unique': "{{field}} debe ser unico, ya existe un registro",
            'range': "{{field}} debe estar entre {{argument.0}} y {{argument.1}}",
            'alpha_numeric': "{{field}} debe ser alfa numérico solamente",
        }
    }
}

module.exports = Seguimiento
