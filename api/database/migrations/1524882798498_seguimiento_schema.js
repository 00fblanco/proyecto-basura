'use strict'

const Schema = use('Schema')

class SeguimientoSchema extends Schema {
  up () {
    this.create('seguimientos', (table) => {
      table.increments()
      table.integer('recolector_id').unsigned().notNullable().references('id').inTable('recolectores');
      table.timestamps()
    })
  }

  down () {
    this.drop('seguimientos')
  }
}

module.exports = SeguimientoSchema
