'use strict'

const Schema = use('Schema')

class UbicacionSchema extends Schema {
  up () {
    this.create('ubicaciones', (table) => {
      table.increments()
      table.decimal('longitud').notNullable()
      table.decimal('latitud').notNullable()
      table.integer('seguimiento_id').unsigned().notNullable().references('id').inTable('seguimientos')
      table.timestamps()
    })
  }

  down () {
    this.drop('ubicaciones')
  }
}

module.exports = UbicacionSchema
