'use strict'

const Schema = use('Schema')

class UbicacionGuardadaSchema extends Schema {
  up () {
    this.create('ubicaciones_guardadas', (table) => {
      table.increments()
      table.string('descripcion').notNullable()
      table.decimal('longitud').notNullable()
      table.decimal('latitud').notNullable()
      table.integer('ciudadano_id').unsigned().notNullable().references('id').inTable('ciudadanos')
      table.timestamps()
    })
  }

  down () {
    this.drop('ubicaciones_guardadas')
  }
}

module.exports = UbicacionGuardadaSchema
