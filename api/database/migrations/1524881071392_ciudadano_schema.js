'use strict'

const Schema = use('Schema')

class CiudadanoSchema extends Schema {
  up () {
    this.create('ciudadanos', (table) => {
      table.increments()
      table.string('nombre').notNullable()
      table.string('ap_paterno').notNullable()
      table.string('ap_materno').notNullable()
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users');
      table.timestamps()
    })
  }

  down () {
    this.drop('ciudadanos')
  }
}

module.exports = CiudadanoSchema
