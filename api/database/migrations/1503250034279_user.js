'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', table => {
      table.increments()
      table.string('correo', 254).notNullable().unique()
      table.string('contrasenia', 60).notNullable()
      table.enu('rol', ['ciudadano', 'recolector']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
