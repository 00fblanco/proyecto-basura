import React from 'react';
import { Root as RootNativeBase } from "native-base";
import Navigator from './routes/Navigator';
import moment from 'moment';

// It's recommended to set locale in entry file globaly.
import '../node_modules/moment/locale/es';
moment.locale('es');

const Root = () =>  <RootNativeBase>
                        <Navigator />
                    </RootNativeBase>

export default Root;