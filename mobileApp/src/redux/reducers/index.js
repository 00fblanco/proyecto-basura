import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import userReducer from './userReducer';
import seguimientoReducer from './seguimientoReducer';

const reducers = {
    form: formReducer,
    user: userReducer,
    seguimientos: seguimientoReducer
}

const allReducers= combineReducers(reducers);
export default allReducers;