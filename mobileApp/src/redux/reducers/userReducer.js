export default function userReducer(state = {}, action){
    switch(action.type){
        case 'LOG_IN':
            return {...state, jwt: action.jwt}
        case 'LOAD_CIUDADANO':
            return {...state, user: {
                id: action.user.id,
                correo: action.user.correo,
                rol: action.user.rol,
                ciudadano: {
                    nombre: action.user.ciudadano.nombre
                }
            }}
        case 'LOAD_RECOLECTOR': 
            return {...state, 
                id: action.user.id,
                email: action.user.email,
                rol: action.user.rol,
                recolector: {
                    nombre: action.user.recolector.nombre,
                    ap_paterno: action.user.recolector.ap_paterno,
                    ap_materno: action.user.recolector.ap_materno
                }
            }
        case 'LOG_OUT':
            return {};
        default:
            return state;
    }
}