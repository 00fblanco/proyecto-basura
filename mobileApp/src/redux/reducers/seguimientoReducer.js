export default function publicationReducer(state = {}, action){
    switch(action.type){
        case 'ADD_SEGUIMIENTO':
            return {
                ...state,
                seguimientos: state.seguimientos.concat([action.seguimiento])
            }
        case 'LOAD_SEGUIMIENTOS':
            return {...action.seguimientos}
        break;
        default: 
            return state;
    }
}