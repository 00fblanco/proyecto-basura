

export function login(jwt){
    return { type: 'LOG_IN', jwt}
}

export function loadRecolector(user, recolector){
    user.recolector = {
        ...recolector
    }
    return { type: 'LOAD_RECOLECTOR', user }
}

export function loadCiudadano(user, ciudadano){
    user.ciudadano = {
        ...ciudadano
    }
    return { type: 'LOAD_CIUDADANO', user }
}

export function logout(){
    return { type: 'LOG_OUT'};
}