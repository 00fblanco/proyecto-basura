
import {  fetchSeguimientos } from '../../constants/requests';

export function addSeguimiento(seguimiento){
    return { type: 'ADD_SEGUIMIENTO', seguimiento}
}
export function loadSeguimientos(seguimientos){
    return { type: 'LOAD_SEGUIMIENTOS', seguimientos}
}

export function getSeguimientos(jwt){
    return (dispatch, getState) => {
        fetchSeguimientos(jwt)
            .then(seguimientos => seguimientos.data)
            .then(seguimientos => {
                dispatch(loadSeguimientos(seguimientos))
            })
    }
}