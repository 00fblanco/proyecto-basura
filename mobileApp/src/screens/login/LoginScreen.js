import React, { Component } from 'react';
import { 
    View,
    Image,
    Dimensions,
    Keyboard,
} from 'react-native';
import { Container, Header, Content, Text, Button, Toast } from 'native-base';
import { Field,reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import Colors from '../../constants/Colors';
import { TextField, Logo } from '../../components';
import { emailValidation, passwordValidation } from '../../validations';
import * as userActions from '../../redux/actions/userActions';
import { login as apiLogin } from '../../constants/requests';

import styles from '../../constants/styles';

const validate = values => {
    const error = {}
    
    var correo = values.correo || '';
    var contrasenia = values.contrasenia || '';

    error.correo =  emailValidation(correo) || '';
    error.contrasenia = passwordValidation(contrasenia) || '';

    return error;
};



class LoginScreen extends Component {
    constructor(props) {
        super(props);

        this.handleLogin = this.handleLogin.bind(this);
        this.loadUser = this.loadUser.bind(this);
        this.goSignup = this.goSignup.bind(this);
    }
    handleLogin(){
        const {  login } = this.props.form;
        if(!login.syncErrors.correo.approved ||  !login.syncErrors.contrasenia.approved){
            return false;
        }
        const { correo, contrasenia } = login.values;
        const credentials = {
            correo,
            contrasenia
        }
        apiLogin(credentials)
            .then(this.loadUser)
            .catch((err) => {
                if(err.response){
                    Toast.show({
                        text: err.response.data.messages,
                        buttonText: "Ok",
                        type: 'warning'
                    })
                }
            })
    }
    loadUser(data){
        const { token, user, recolector, ciudadano } = data.data;
        this.props.login(token.token);
        if(recolector){
            this.props.loadRecolector(user, recolector);
            this.props.navigation.navigate('recolector');
        }else if(ciudadano){
            this.props.loadCiudadano(user, ciudadano);
            this.props.navigation.navigate('ciudadano')
        }
        
    }
    componentWillMount() {
        const correo = this.props.navigation.getParam('correo', '');
        this.props.initialize({ correo })
    }
    goSignup(){
        this.props.navigation.navigate('Signup');
    }
    render() {
        return (
            <Container style={{backgroundColor: 'white'}}>
                <Content style={{marginHorizontal: 15}}>
                    <View style={styles.imageContainer}>
                        <Logo />
                        <Text style={[styles.light, {fontSize: 30, paddingVertical: 10}]}>Tlatzo</Text>
                    </View>
                    <View style={[{flex: 1} ]}>
                        <Field label="Correo electrónico" name="correo" component={TextField} keyboardType="email-address" />
                        <Field label="Contraseña" name="contrasenia" secure={true} component={TextField} />
                        <View style={{flex: 1, flexDirection: 'row',  marginTop: 20, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={[styles.light]}>¿No tienes una cuenta?</Text>
                            <Button transparent onPress={() => this.goSignup()}>
                                <Text style={[styles.light, { color: Colors.$primaryColor}]}> Registrate </Text>
                            </Button>
                        </View>
                        <Button onPress={() => this.handleLogin()} rounded  style={[styles.shadowButton, {backgroundColor: Colors.$primaryColor, alignSelf: 'center', justifyContent: 'center', marginVertical: 20, shadowColor: Colors.$primaryColor, width: '90%'}]}>
                            <Text style={[ styles.light,{ fontSize: 20}]}>Inicia sesión</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

function mapStateToProps (state, ownProps){
    return {
        form: state.form
    }
}
export default 
    reduxForm({
        form: 'login',
        validate,
    })(connect(mapStateToProps, { login: userActions.login, loadCiudadano: userActions.loadCiudadano, loadRecolector: userActions.loadRecolector })(LoginScreen))
