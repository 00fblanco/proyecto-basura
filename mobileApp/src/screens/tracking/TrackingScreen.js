import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    Icon,
    Container,
    Content,
    Button,
    Text,
    List,
    ListItem,
    Left,
    Body,
    Right,
    Switch,
    Toast
} from 'native-base';
import moment from 'moment';
import io from 'socket.io-client';

import * as seguimientoActions from '../../redux/actions/seguimientoActions'
import { storeSeguimiento, IP } from '../../constants/requests.js'
import { sendPosition } from '../../constants/sockets';
import Colors from '../../constants/Colors';
import styles from '../../constants/styles';

class TrackingScreen extends Component {
    constructor(props){
        super(props);

        this.loadSeguimientos = this.loadSeguimientos.bind(this);
        this.addSeguimiento = this.addSeguimiento.bind(this);
        this.loadSeguimientos();
        this.socket = io(`http://${IP}/`, {
            transportOptions: {
                polling:{
                    extraHeaders: {
                        'Authorization': this.props.user.jwt
                    }
                }
            }
        });
        // send position
        navigator.geolocation.watchPosition((watch) => {
            console.log('position', watch.coords);
            sendPosition(this.socket, watch.coords);
        });
    }
    loadSeguimientos(){
        this.props.getSeguimientos(this.props.user.jwt);
    }
    static navigationOptions = {
        headerStyle: {
            backgroundColor: Colors.$primaryColor,
        },
        title: 'Seguimientos',
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: '400',
            fontSize: 20
        },
        tabBarIcon: ({ tintColor }) => (
            <Icon ios="ios-pin" android="md-pin" size={25} style={{ color: tintColor }}/>
        )
    }
    addSeguimiento(){
        storeSeguimiento(this.props.user.jwt)
            .then((data) => {
                this.props.addSeguimiento(data.data.seguimiento);
            })
            .then(() => {
                // ws.connect();
                // const wsUbicacion = ws.subscribe('ubicacion');

                // navigator.geolocation.getCurrentPosition(position => {
                //     wsUbicacion.emit('ubicacion', position)
                // });
                
                
            })
            .catch((err) => {
                if(err.response){
                    Toast.show({
                        text: err.response.data.messages,
                        buttonText: "Ok",
                        type: 'warning'
                    })
                }
            })
    }

    render() {
        const { seguimientos } = this.props;
        return (
            <Container style={{backgroundColor: Colors.$grayColor}}>
                <Content>
                    <Button onPress={() => this.addSeguimiento()} rounded  style={[styles.shadowButton, {backgroundColor: Colors.$darkColor, alignSelf: 'center', justifyContent: 'center', marginTop: 20, shadowColor: Colors.$darkColor, marginBottom: 20}]}>
                        <Icon ios="ios-play" android="md-play" />
                        <Text style={[ styles.bold,{ fontSize: 18}]}>Agregar seguimiento</Text>
                    </Button>
                    <Text style={[styles.light, { marginVertical: 20, marginHorizontal: 10}]}>Historial de seguimientos</Text>
                    {
                        Object.keys(seguimientos).length === 0 ?
                            <Text>cargando...</Text>
                            :
                            <List>
                                {seguimientos.seguimientos.map((seguimiento, key) => {

                                    return (
                                        <ListItem key={key}>
                                            <Left>
                                                <Icon ios="ios-pin" android="md-pin" />
                                            </Left>
                                            <Body>
                                                <Text style={{textAlign: 'center', alignSelf: 'center'}}>{moment(seguimiento.created_at).utc().utcOffset(-5).format('MMMM DD YYYY hh:mm:ss')}</Text>
                                            </Body>
                                        </ListItem>
                                    )
                                })}
                            </List>
                            
                    }
                </Content>
            </Container>
        );
    }
}

function mapStateToProps(state, ownProps){
    return {
        user: state.user,
        seguimientos: state.seguimientos
    }
}
export default connect(mapStateToProps, { getSeguimientos: seguimientoActions.getSeguimientos, addSeguimiento: seguimientoActions.addSeguimiento })(TrackingScreen);