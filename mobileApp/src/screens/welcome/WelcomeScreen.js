import React, { Component } from 'react';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import {
    View, Text,
    Platform
} from 'react-native';
import {
    Icon
} from 'native-base';
import io from 'socket.io-client';

import { IP } from '../../constants/requests.js'
import { getPosition } from '../../constants/sockets';
import Colors from '../../constants/Colors';

class WelcomeScreen extends Component {
    constructor(props){
        super(props);
        this.setLocation = this.setLocation.bind(this);

        // this.socket = io(`http://${IP}/`);
        // this.socket.on('ubicacion', (data) => {
        //     this.setLocation(data)
        // })
    }

    state = {
        latitude: 20.9948891,
        longitude: 105.799677,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
    }
    setLocation(data){
        this.setState({
            latitude: data.latitude,
            longitude: data.longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
        })
    }

    static navigationOptions = {
        headerStyle: {
            backgroundColor: Colors.$primaryColor,
        },
        title: 'Welcome',
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: '400',
            fontSize: 20
        },
        tabBarIcon: ({ tintColor }) => (
            <Icon ios="ios-home" android="md-home" size={25} style={{ color: tintColor }}/>
            
        )
    }
    render() {
        return (
            <View style={{flex: 1}}>
                <Text>Welcome Ciudadano</Text>
                <MapView 
                    showsUserLocation={true}
                    initialRegion={this.state}
                    style={styles.map}>
                </MapView>

            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    text: {
        fontSize: 30,
        fontWeight: '700',
        color: '#59656C',
        marginBottom: 10,
    },
    map: {
        width: null,
        height: 300,
        flex: 1
    }
};

export default WelcomeScreen;