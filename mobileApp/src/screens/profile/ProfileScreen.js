import React, { Component } from 'react';
import {
    Text,
    View
} from 'react-native';
import {
    Icon
} from 'native-base';
import Colors from '../../constants/Colors';

class ProfileScreen extends Component {
    static navigationOptions = {
        headerStyle: {
            backgroundColor: Colors.$primaryColor,
        },
        title: 'Perfil',
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: '400',
            fontSize: 20
        },
        tabBarIcon: ({ tintColor }) => (
            <Icon ios="ios-contact" android="md-contact" size={25} style={{ color: tintColor }}/>
            
        )
    }
    render() {
        return (
            <View style={{flex: 1}}>
                <Text>Profile Screen</Text>    
            </View>
        );
    }
}

export default ProfileScreen;