import React, { Component } from 'react';
import { 
    View,
    Image,
    Dimensions,
    Keyboard,
} from 'react-native';
import { Container, Header, Content, Text, Button, Toast } from 'native-base';
import { Field,reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import Colors from '../../constants/Colors';
import { TextField, Logo } from '../../components';
import { emailValidation, passwordValidation, palabraValidation} from '../../validations';
import * as userActions from "../../redux/actions/userActions";
import { storeCiudadano } from "../../constants/requests";


import styles from '../../constants/styles';

const validate = values => {
    const error = {}
    
    const correo = values.correo || '';
    const contrasenia = values.contrasenia || '';
    const nombre = values.nombre || '';
    const ap_paterno = values.ap_paterno || '';
    const ap_materno = values.ap_materno || '';

    error.correo =  emailValidation(correo);
    error.contrasenia = passwordValidation(contrasenia);
    error.nombre = palabraValidation(nombre);
    error.ap_paterno = palabraValidation(ap_paterno);
    error.ap_materno = palabraValidation(ap_materno)

    return error;
};

class SignupScreen extends Component {
    constructor(props) {
        super(props);

        this.handleSignup = this.handleSignup.bind(this);
        this.loadUser = this.loadUser.bind(this);
        this.goLogin = this.goLogin.bind(this);
    }
    handleSignup(){
        const {  signup } = this.props.form;
        if(!signup.syncErrors.correo.approved ||  !signup.syncErrors.contrasenia.approved){
            return false;
        }
        const { correo, contrasenia, nombre, ap_paterno, ap_materno } = signup.values;
        const data = {
            correo,
            contrasenia,
            nombre, 
            ap_paterno,
            ap_materno
        }
        storeCiudadano(data)
        .then(data => data.data)
        .then(this.loadUser)
        .catch(err => {
            if (err.response) {
                console.log(err.response.data.messages[0].message);
                Toast.show({
                    text: err.response.data.messages[0].message,
                    buttonText: "Ok",
                    type: "warning"
                });
            }
        });

    }
    loadUser(data){
        Toast.show({
            text: 'Ciudadano agregado, inicie sesión...',
            buttonText: 'Ok',
            type: 'success'
        })
        this.props.navigation.navigate('Login', { correo: data.user.correo });
    }

    goLogin(){
        this.props.navigation.navigate('Login');
    }
    render() {
        return (
            <Container style={{backgroundColor: Colors.$grayColor}}>
                <Content style={{marginHorizontal: 15}}>
                    <View style={styles.imageContainer}>
                        <Logo />
                    </View>
                    <View style={[{flex: 1}, styles.box]}>
                        <Field label="Correo electrónico" name="correo" component={TextField} keyboardType="email-address" />
                        <Field label="Contraseña" name="contrasenia" secure={true} component={TextField} />
                        <Field label="Nombre" name="nombre" component={TextField} />
                        <Field label="Apellido paterno" name="ap_paterno" component={TextField} />
                        <Field label="Apellido materno" name="ap_materno" component={TextField} />

                        <View style={{flex: 1, flexDirection: 'row',  marginTop: 20, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={[styles.light]}>Si ya tienes cuenta, </Text>
                            <Button transparent onPress={() => this.goLogin()}>
                                <Text style={[styles.light, { color: Colors.$primaryColor, paddingLeft: 5}]}>Inicia sesión</Text>
                            </Button>
                        </View>
                        <Button onPress={() => this.handleSignup()} rounded  style={[styles.shadowButton, {backgroundColor: Colors.$primaryColor, alignSelf: 'center', justifyContent: 'center', marginTop: 20, shadowColor: Colors.$primaryColor, width: '90%'}]}>
                            <Text style={[ styles.light,{ fontSize: 20}]}>Registrate</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

function mapStateToProps (state, ownProps){
    return {
        form: state.form
    }
}
export default 
    reduxForm({
        form: 'signup',
        validate
    })(connect(mapStateToProps, {})(SignupScreen))
