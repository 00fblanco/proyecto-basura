import React from 'react';
import { Image } from 'react-native';
const Logo = () => {
    return (
        <Image source={require('../../assets/images/logo.png')}/>
    );
};

export default Logo;