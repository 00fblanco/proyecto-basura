import React from 'react';
import { Input, Item, Text } from 'native-base';
import Colors from '../constants/Colors';

const TextField = ({ input, label, type, value, secure, keyboardType = 'default', meta: { touched, error, warning } } ) => {
    var hasError = false;
    if(error.errors.length !== 0){
        hasError = true;
    }
    
    return( 
        <Item  style={{marginVertical: 10}} rounded>
            <Input keyboardType={keyboardType}  secureTextEntry={secure} placeholder={label} {...input}/>
            {hasError ? <Text style={{color: Colors.$redColor, marginRight: 5, fontFamily: 'Montserrat-Light',}}>{error.errors[0]}</Text> : null}
        </Item>
    )
}

export default TextField;