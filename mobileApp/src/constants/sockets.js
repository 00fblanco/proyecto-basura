import { IP } from './requests';

function sendPosition( socket, position ){
    return socket.emit('ubicacion', position)
}

export {
    sendPosition
}