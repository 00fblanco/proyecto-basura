export default {
  $blackColor: '#282c34',
  $whiteColor: '#fff',
  $grayColor: '#f3f3f3',
  $darkGrayColor: '#bababa',
  $primaryColor: '#0bcc9d',
  $lightColor: '#63ffce',
  $darkColor: '#00b386',
  $redColor: '#ff5757',
};
