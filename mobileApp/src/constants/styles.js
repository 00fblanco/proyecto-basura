import EStyleSheet from 'react-native-extended-stylesheet';
import { Platform } from 'react-native';

import Colors from './Colors';
const styles = EStyleSheet.create({
  halfModal: {
    height: "80%" ,
    width: '100%', 
    backgroundColor:"#fff", 
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  box: {
    elevation: 5, 
    backgroundColor: Colors.$whiteColor, 
    borderRadius: 20, 
    marginVertical: 30, 
    paddingHorizontal: 10, 
    paddingVertical: 30 
    ,shadowColor: '#282c34',
    shadowOffset: {
      width: 0,
      height: 10
    },
    shadowRadius: 5,
    shadowOpacity: 0.0
  },
  shadowButton: {
    shadowOffset: {
      width: 5,
      height: 10
    },
    elevation: 10,
    shadowRadius: 5,
    shadowOpacity: 0.2
  },
  light: {
    fontFamily: 'Montserrat-Light',
  },
  bold: {
    fontFamily: 'Montserrat-Bold',
  },
  imageContainer: {
    flex: 1, 
    marginTop: 100, 
    alignItems: 'center'
  }
});

export default styles;