import axios from 'axios';
import { Platform } from 'react-native';

const IP = Platform.OS === 'ios' ? '127.0.0.1:3000' : '10.0.3.2:3000';

axios.defaults.baseURL = `http://${IP}/api`;

function login(credentials){
    return axios.post('/users/auth', credentials)
}
function fetchSeguimientos(jwt) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`;
    return axios.get('/seguimientos')
}

function storeSeguimiento(jwt){
    axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`;
    return axios.post('/seguimientos')
}
function storeCiudadano(ciudadano) {
    return axios.post("/ciudadanos", ciudadano);
}
  
export { IP, login, fetchSeguimientos, storeSeguimiento, storeCiudadano }
