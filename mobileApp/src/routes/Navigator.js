import { StackNavigator } from 'react-navigation'

import TabNavigatorCiudadano from './TabNavigatorCiudadano';
import TabNavigatorRecolector from './TabNavigatorRecolector';

import { 
    SignupScreen,
    LoginScreen
} from '../screens';

import ModalPublicationDetail from '../components/ModalPublicationDetail';

// ciudadano stack
const CiudadanoStack = StackNavigator({
  AppTabNavigator: {
    screen: TabNavigatorCiudadano,
    navigationOptions: {
      headerLeft: null,
    }
  },
}, 
  {
  navigationOptions:{
    gesturesEnabled: false
  }
});

// recolector stack
const RecolectorStack = StackNavigator({
  AppTabNavigator: {
    screen: TabNavigatorRecolector,
    navigationOptions: {
      headerLeft: null,
    }
  },
}, 
  {
  navigationOptions:{
    gesturesEnabled: false
  }
});

export default StackNavigator({
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        header: null
      }
    },
    Signup: {
      screen: SignupScreen,
      navigationOptions: {
        header: null
      }
    },
    ciudadano: {
      screen: CiudadanoStack
    },
    recolector: {
      screen: RecolectorStack
    }
}, {
  headerMode: 'none'
})
