import React from 'react';

import { TabNavigator } from 'react-navigation';
import { Text, View } from 'react-native';
import Colors from '../constants/Colors';

import {
    WelcomeScreen,
    TrackingScreen,
    ProfileScreen
} from '../screens';

export default TabNavigator({
    Welcome: {
        screen: WelcomeScreen
    },
    Tracking: {
        screen: TrackingScreen
    },
    Profile: {
        screen: ProfileScreen
    }
}, {    
    swipeEnabled: true,
    animationEnabled: true,
    tabBarPosition: 'bottom',
    tabBarOptions: {
        showLabel: false,
        showIcon: true, // para visualizar el tab en android
        inactiveTintColor: Colors.$darkGrayColor,
        activeTintColor: Colors.$primaryColor,
        indicatorStyle: { backgroundColor: Colors.$darkColor },
        pressColor: Colors.$darkColor,
        style: {
            backgroundColor: Colors.$whiteColor
        }
    }
});
