import React, { Component } from 'react';
import { Provider } from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';

import { store } from './src/redux/store/';
import Root from './src/Root';
import Colors from './src/constants/Colors';
EStyleSheet.build(Colors);


export default class App extends Component {
  render() {
    return (<Provider store={store}>
              <Root />
            </Provider>);
    }
}